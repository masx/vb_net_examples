Imports System

Module Program
    Sub Main(args As String())
        Dim auto = New Automobile With {
                .Make = "Toyota",
                .Model = "4Runner",
                .Year = 2017
            }
        auto.Drive()
        Dim truck = New Truck() With {
                .Make = "Toyota",
                .Model = "Tundra",
                .Year = 2016,
                .TowingCapacity = 1000
            }
        truck.Drive()
        truck.Tow()
        Console.ReadLine()
    End Sub
End Module
