﻿Class Truck
    Inherits Automobile

    Public Property TowingCapacity As Integer
    Public Property CargoSize As Integer

    Public Sub Tow()
        Console.WriteLine($"Now we're towing {TowingCapacity} pounds!")
    End Sub
End Class