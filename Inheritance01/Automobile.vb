﻿Class Automobile
    Public Property Make As String
    Public Property Model As String
    Public Property Year As Integer

    Public Sub Drive()
        Console.WriteLine($"The {Year} {Make} {Model} is driving.")
    End Sub
End Class